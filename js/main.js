var dssv = [];
const DSSV = "DSSV";

var dataJson = localStorage.getItem(DSSV);
if (dataJson) {
  var dataRaw = JSON.parse(dataJson);

  //   // way 1:
  //   var result = [];
  //   for (var index = 0; index < dataRaw.length; index++) {
  //     var currentData = dataRaw[index];
  //     var sv = new SinhVien(
  //       currentData.ma,
  //       currentData.ten,
  //       currentData.email,
  //       currentData.matkhau,
  //       currentData.diemLy,
  //       currentData.diemToan,
  //       currentData.diemHoa
  //     );
  //     result.push(sv);
  //   }
  //   dssv = result;

  // way 2:
  dssv = dataRaw.map(function (item) {
    return new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matkhau,
      item.diemLy,
      item.diemToan,
      item.diemHoa
    );
  });
  renderDssv(dssv);
}

// create a function to save Local Storage
function saveLocalStorage() {
  var dssvJson = JSON.stringify(dssv);
  localStorage.setItem(DSSV, dssvJson);
  console.log("dssvJson: ", dssvJson);
}

// function to add Student
function themSV() {
  var newSv = layThongTinTuForm();
  var isValid = true;
  // check empty and check correct Value
  isValid &=
    kiemTraRong(newSv.ma, "spanMaSV") &&
    kiemTraMaSV(newSv.ma, dssv, "spanMaSV");
  isValid &= kiemTraRong(newSv.ten, "spanTenSV");
  isValid &=
    kiemTraRong(newSv.email, "spanEmailSV") &&
    kiemTraEmail(newSv.email, "spanEmailSV");

  // add student
  if (isValid) {
    dssv.push(newSv);
    saveLocalStorage();
    renderDssv(dssv);
    resetForm();
  }
}

function xoaSV(idSV) {
  // find id of student
  var index = dssv.findIndex(function (sv) {
    return sv.ma == idSV;
  });
  if (index == -1) return;

  // delete student from array
  dssv.splice(index, 1);
  console.log("dssv: ", dssv);

  // render student array which is updated and save on localStorage again
  renderDssv(dssv);
  saveLocalStorage();
}

function suaSV(idSV) {
  // find id of student
  var index = dssv.findIndex(function (sv) {
    return sv.ma == idSV;
  });
  if (index == -1) return;

  var sv = dssv[index];
  showThongTinTuForm(sv);
  document.getElementById("txtMaSV").disabled = true;
}

function capNhatSV() {
  var svEdit = layThongTinTuForm();

  // find id of student
  var index = dssv.findIndex(function (sv) {
    return sv.ma == svEdit.ma;
  });
  if (index == -1) return;

  dssv[index] = svEdit;
  saveLocalStorage();
  renderDssv(dssv);
  resetForm();
  document.getElementById("txtMaSV").disabled = false;
}

function searchSV(value, listSv) {
  var filteredData = [];

  for (var i = 0; i < listSv.length; i++) {
    value = value.toLowerCase();
    var name = listSv[i].ten.toLowerCase();
    if (name.includes(value)) {
      filteredData.push(listSv[i]);
    }
  }
  return filteredData;
}

document.getElementById("btnSearch").onclick = function () {
  var letValue = document.getElementById("txtSearch").value.trim();
  console.log("letValue: ", letValue);

  var searchName = searchSV(letValue, dssv);
  renderDssv(searchName);
};
