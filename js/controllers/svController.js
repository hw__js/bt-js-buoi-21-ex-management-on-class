function layThongTinTuForm() {
  var maSV = document.getElementById("txtMaSV").value.trim();
  var tenSV = document.getElementById("txtTenSV").value.trim();
  var email = document.getElementById("txtEmail").value.trim();
  var matkhau = document.getElementById("txtPass").value.trim();
  var diemLy = document.getElementById("txtDiemLy").value.trim();
  var diemToan = document.getElementById("txtDiemToan").value.trim();
  var diemHoa = document.getElementById("txtDiemHoa").value.trim();

  // create object that information is let from the form
  var sv = new SinhVien(maSV, tenSV, email, matkhau, diemLy, diemToan, diemHoa);
  return sv;
}

function renderDssv(list) {
  var contentHTML = "";

  for (var i = 0; i < list.length; i++) {
    var currentSv = list[i];
    var contentTr = `<tr>
            <td>${currentSv.ma}</td>
            <td>${currentSv.ten}</td>
            <td>${currentSv.email}</td>
            <td>${currentSv.tinhDTB().toFixed(2)}</td>
            <td>
                <button class = "btn btn-primary" onclick="suaSV('${
                  currentSv.ma
                }')">Edit</button>
                <button class = "btn btn-danger" onclick="xoaSV('${
                  currentSv.ma
                }')">Delete</button>
            </td>
        </tr>`;
    contentHTML += contentTr;
  }

  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function showThongTinTuForm(sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matkhau;
  document.getElementById("txtDiemLy").value = sv.diemLy;
  document.getElementById("txtDiemToan").value = sv.diemToan;
  document.getElementById("txtDiemHoa").value = sv.diemHoa;
}

function resetForm() {
  document.getElementById("formQLSV").reset();
  document.getElementById("txtMaSV").disabled = false;
}
